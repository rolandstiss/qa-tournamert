﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelControllerScript : MonoBehaviour
{
    public List<GameObject> particles;
    public Transform[] spawnPoints;
    public GameObject[] pickups;

    NetworkManagerClient nmc;

    private void OnEnable()
    {
        nmc = GameObject.Find("NetworkManagerClient").GetComponent<NetworkManagerClient>();
        SceneManager.sceneLoaded += LevelIsLoaded;
    }

    // Use this for initialization
    void Start () {
        
    }

    // Update is called once per frame
    void Update () {
		
	}

    private void LevelIsLoaded(Scene scene, LoadSceneMode mode)
    {
        nmc.SendToServer("LEVELLOADED|", nmc.myReliableChannel);
    }
}
