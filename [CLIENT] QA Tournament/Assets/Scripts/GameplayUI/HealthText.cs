﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class HealthText : MonoBehaviour {

    int pId;
    Text healthText;

	// Use this for initialization
	void Start () {
        pId = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().playerId;
        healthText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        healthText.text = GameControllerScript.playerList[pId].health.ToString();
	}
}
