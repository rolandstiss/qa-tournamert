﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using UnityEngine;

public class WeaponIcons : MonoBehaviour {

    public List<Sprite> wpIcons;
    Image img;
    WeaponSwitching ws;
    int wpId;

	// Use this for initialization
	void Start () {
        ws = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<WeaponSwitching>();
        img = GetComponent<Image>();
        wpId = ws.weaponId;
        img.sprite = wpIcons.ElementAt(wpId);
    }
	
	// Update is called once per frame
	void Update () {
        if (wpId != ws.weaponId)
        {
            wpId = ws.weaponId;
            img.sprite = wpIcons.ElementAt(wpId);
            Debug.Log(wpId);
        }
	}
}
