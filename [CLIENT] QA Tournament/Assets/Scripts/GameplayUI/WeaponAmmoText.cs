﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using UnityEngine;

public class WeaponAmmoText : MonoBehaviour {

    Text ammoText;
    Inventory inv;
    WeaponSwitching ws;

	// Use this for initialization
	void Start () {
        ammoText = GetComponent<Text>();
        inv = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        ws = GameObject.FindGameObjectWithTag("Player").GetComponent<WeaponSwitching>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!inv.inventory.ElementAt(ws.weaponId).isReloaded && inv.inventory.ElementAt(ws.weaponId).reserve > 0)
            ammoText.text = "RELOADING!";
        else if(inv.inventory.ElementAt(ws.weaponId).reserve == 0 && inv.inventory.ElementAt(ws.weaponId).ammo == 0)
            ammoText.text = "OUT OF AMMO";
        else
            ammoText.text = inv.inventory.ElementAt(ws.weaponId).ammo.ToString() + "/" + inv.inventory.ElementAt(ws.weaponId).reserve.ToString();
    }
}
