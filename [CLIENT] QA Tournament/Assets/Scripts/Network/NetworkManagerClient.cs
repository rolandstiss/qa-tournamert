﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using UnityEngine.UI;

public class NetworkManagerClient : MonoBehaviour {

    const int MAX_CONNECTIONS = 1;

    int port = 5071;
    int hostId;
    public int myReliableChannel;
    public int myUnreliableChannel;
    int connectionId;
    int prefabId;
    public int ourPlayerId;

    string connectionIP = "127.0.0.1";
    string playerName;

    //public bool isConnecting = false;
    public bool isConnected = false;
    public bool nameExists = false;

    public byte error;

    public List<GameObject> prefabs = new List<GameObject>();

    List<Transform> sp = new List<Transform>(); //Spawn Points from GameControllerScript

    GameObject hangarUI;
    GameObject turntable;
    GameObject gameUI;

    public List<GameObject> particles;
    public List<Pickup> collectables;

    GameControllerScript gc;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        hangarUI = GameObject.Find("HangarUI");
        //gameUI = GameObject.Find("GameUI");
        turntable = GameObject.Find("Turntable");
        gc = GameObject.Find("GameController").GetComponent<GameControllerScript>();

        collectables = new List<Pickup>();

        //gameUI.SetActive(false);

        //foreach (Transform s in gc.spawnPoints)
        //{
        //    sp.Add(s);
        //}

        //Debug.Log(GameObject.Find("GameController").GetComponent<GameControllerScript>().spawnPoints);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isConnected)
            return;

        ReceivedData();
    }

    void ReceivedData()
    {
        int recHostId;
        int connectionId;
        int channelId;
        byte[] recBuffer = new byte[1024];
        int bufferSize = 1024;
        int dataSize;
        //byte error;
        NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
        switch (recData)
        {
            case NetworkEventType.DataEvent:
                string recDataString = Encoding.Unicode.GetString(recBuffer);
                string[] splitData = recDataString.Split('|');
                switch (splitData[0])
                {
                    case "ASKNAME":
                        //0 - ASKNAME, 1 - cnnId
                        ourPlayerId = int.Parse(splitData[1]);
                        OnAskName(ourPlayerId);
                        break;

                    case "NAMEEXISTS":
                        Debug.Log("nameExists");
                        nameExists = true; //should return to the hangar!
                        isConnected = false;
                        NetworkTransport.Disconnect(recHostId, connectionId, out error);
                        break;

                    case "MATCHSTART":
                        SceneManager.LoadScene("Client_Level");
                        break;

                    case "NEWPLAYER":
                        OnNewPlayer(recDataString);
                        break;

                    case "ALLPLAYERS":
                        OnAllPlayers(recDataString);
                        break;

                    case "DC":
                        RemovePlayer(int.Parse(splitData[1]));
                        break;

                    case "PDC":
                        Debug.Log("PDC");
                        
                        NetworkTransport.Disconnect(hostId, connectionId, out error);
                        GameControllerScript.playerList.Clear();
                        if (SceneManager.GetActiveScene() != SceneManager.GetSceneByName("Client_Hangar"))
                        {
                            SceneManager.LoadScene(0);
                        }

                        break;

                    case "JUSTMSG":
                        Debug.Log("JUSTMSG");
                        break;

                        //case "PLAYERSTATS":
                        //    SpawnPlayer(int.Parse(splitData[1]), splitData[2], int.Parse(splitData[3]),
                        //                    sp[int.Parse(splitData[4])].position.x,
                        //                    sp[int.Parse(splitData[4])].position.z,
                        //                    sp[int.Parse(splitData[4])].rotation.y,
                        //                    0, int.Parse(splitData[5]), 0);
                        //    break;

                        //case "STILLINLOBBY":
                        //    break;

                        //case "CNN":
                        //    //Debug.Log("splitData[5]: " + splitData[5]);
                        //    SpawnPlayer(int.Parse(splitData[1]), splitData[2], int.Parse(splitData[3]), 
                        //                sp[int.Parse(splitData[4])].position.x,
                        //                sp[int.Parse(splitData[4])].position.z,
                        //                sp[int.Parse(splitData[4])].rotation.y,
                        //                0, int.Parse(splitData[5]), 0);
                        //    break;

                        //case "ASKPOSITION":
                        //    //Debug.Log("ask pos");
                        //    OnAskPosition(splitData);
                        //    break;



                        //case "WPNSW":
                        //    //Debug.Log("WPNSW");
                        //    OnWeaponSwitch(int.Parse(splitData[1]), int.Parse(splitData[2]));
                        //    break;

                        //case "SPWNPART":
                        //    if (splitData[1] != ourPlayerId.ToString())
                        //    {
                        //        GameObject p = Instantiate(particles.ElementAt(int.Parse(splitData[2])),
                        //                                                    new Vector3(float.Parse(splitData[3]), float.Parse(splitData[4]), float.Parse(splitData[5])),
                        //                                                    new Quaternion(float.Parse(splitData[6]), float.Parse(splitData[7]), float.Parse(splitData[8]), float.Parse(splitData[9]))
                        //                                                    );
                        //    }
                        //    break;

                        //case "DAMAGE":
                        //    //Debug.Log("damage!");
                        //    GameControllerScript.playerList[int.Parse(splitData[1])].health = int.Parse(splitData[2]); 
                        //    break;

                        //case "RESPAWN":
                        //    //Debug.Log("RESPAWN");
                        //    GameControllerScript.playerList[int.Parse(splitData[1])].health = int.Parse(splitData[5]);
                        //    GameControllerScript.playerList[int.Parse(splitData[1])].pos = new Vector3(float.Parse(splitData[2]), 0.0f, float.Parse(splitData[3]));
                        //    break;

                        //case "ENEMYELIMINATED":
                        //    break;

                        //case "SPAWNPICKUPS":
                        //    //Debug.Log("spawnpickups");
                        //    SpawnPickups(splitData);
                        //    break;

                        //case "DISABLEPICKUP":
                        //    collectables.ElementAt(int.Parse(splitData[1])-1).isCollected = true;
                        //    break;

                        //case "REENABLEPICKUP":
                        //    collectables.ElementAt(int.Parse(splitData[1]) - 1).isCollected = false;
                        //    break;
                }

                break;
        }
    }

    //Initialize connection to the server
    public void ConnectToServer(string pName, int prefId)
    {
        NetworkTransport.Init();

        ConnectionConfig config = new ConnectionConfig();
        myReliableChannel = config.AddChannel(QosType.Reliable);
        myUnreliableChannel = config.AddChannel(QosType.Unreliable);

        HostTopology topology = new HostTopology(config, MAX_CONNECTIONS);
        hostId = NetworkTransport.AddHost(topology, 0);
        connectionId = NetworkTransport.Connect(hostId, connectionIP, port, 0, out error);

        playerName = pName;
        prefabId = prefId;

        isConnected = true;
        
        

        //if (connectionId != 0)
        //{
        //    playerName = pName;
        //    prefabId = prefId;
        //    Debug.Log("cnnId: " + connectionId);
        //    //isConnected = true;
        //}
        //else
        //    Debug.Log("Try Again!");        
    }

    //After connection send player info to the server
    //msg = command|connection id|playerName|prefabId
    void OnAskName(int pId)
    {
        string msg = "NAMEIS|" + pId + '|' + playerName + '|' + prefabId;
        SendToServer(msg, myReliableChannel);
    }

    public void SendToServer(string message, int chId)
    {
        byte[] msg = Encoding.Unicode.GetBytes(message);
        NetworkTransport.Send(hostId, connectionId, chId, msg, msg.Length * sizeof(char), out error);
    }

    void SpawnPlayer(int cnnId, string pName, int prefId, float posX, float posZ, float rotY, int wpId, int health, int points) //Register player
    {
        //Debug.Log("wpId spwn pl: " + wpId);

        Vector3 pos = new Vector3(posX, 0.0f, posZ);
        Quaternion rot = Quaternion.Euler(0.0f, rotY, 0.0f);

        //GameObject pl = Instantiate(prefabs.ElementAt(prefId), pos, rot) as GameObject;

        Player p = new Player();
        p.connectionId = cnnId;
        p.prefabId = prefId; // is this neccessary?!
        p.weaponId = wpId;
        p.health = 100;
        p.points = points;
        p.loses = 0;

        p.playerName = pName;

        //p.avatar = pl;

        p.pos = pos;
        p.rot = rot;

        GameControllerScript.playerList.Add(p.connectionId, p);

        //if this is client's palyer the movement script should be added!!!
        //if (cnnId == ourPlayerId)
        //{
        //    pl.layer = LayerMask.NameToLayer("Player");
        //    pl.tag = "Player";

        //    pl.AddComponent<PlayerMovement>();
        //    pl.AddComponent<WeaponSwitching>();
        //    pl.AddComponent<Inventory>();
        //    pl.AddComponent<WeaponReload>();
        //    pl.AddComponent<Shooting>().playerId = ourPlayerId;
        //    pl.AddComponent<PlayerController>().playerId = ourPlayerId;

        //    GameObject.Find("ImageWeapon").GetComponent<WeaponIcons>().enabled = true;
        //    GameObject.Find("TextHealth").GetComponent<HealthText>().enabled = true;
        //}
        //else
        //{
        //    pl.AddComponent<EnemyController>().enemyId = cnnId;
        //    pl.AddComponent<EnemyInterpolation>();
        //    pl.transform.Find("WeaponHolder").gameObject.AddComponent<WeaponSync>();//.weaponId;// = wpId;
        //    pl.layer = LayerMask.NameToLayer("Enemy");
        //    pl.tag = "Enemy";
        //}
    }

    void RemovePlayer(int cnnId)
    {
        //Destroy(GameControllerScript.playerList[cnnId].avatar);
        GameControllerScript.playerList.Remove(cnnId);
    }

    void OnAskPosition(string[] data)
    {
        for (int i = 1; i < data.Length; i++)
        {
            string[] d = data[i].Split('%');
            if (int.Parse(d[0]) != ourPlayerId) //prevents from my client update
            {
                GameControllerScript.playerList[int.Parse(d[0])].pos = new Vector3(float.Parse(d[1]), 0.0f, float.Parse(d[2]));
                GameControllerScript.playerList[int.Parse(d[0])].rot = Quaternion.Euler(0.0f, float.Parse(d[3]), 0.0f);
            }
        }

        Vector3 myPos = GameControllerScript.playerList[ourPlayerId].avatar.transform.position;
        Quaternion myRot = GameControllerScript.playerList[ourPlayerId].avatar.transform.rotation;

        string m = "MYPOSITION|" + ourPlayerId + '|' + myPos.x.ToString() + '|' + myPos.z.ToString() + '|' + myRot.eulerAngles.y.ToString();
        SendToServer(m, myUnreliableChannel);
    }

    void SyncTime(float t)
    {
        //GameObject.Find("TextTime").GetComponent<TimerScript>().SetTime(t);
    }

    void OnWeaponSwitch(int cnnId, int wpId)
    {
        GameControllerScript.playerList[cnnId].weaponId = wpId;
        //Debug.Log("player: " + cnnId + " has weapon number: " + wpId);
    }

    void SpawnPickups(string[] data)
    {
        string[] pickupData;

        for (int i = 1; i < data.Length; i++)
        {
            pickupData = data[i].Split('%');

            Pickup p = new Pickup();
            p.id = int.Parse(pickupData[0]);
            p.type = int.Parse(pickupData[1]);
            p.isCollected = bool.Parse(pickupData[2]);
            p.location = StringToVector(pickupData[3]);
            p.additionalAmmo = int.Parse(pickupData[4]);

            //Debug.Log("type: " + p.type);

            //GameObject collectable = Instantiate(gc.pickups[p.type], p.location, Quaternion.identity) as GameObject;
            //collectable.AddComponent<PickupSync>().id = p.id;
            //collectable.GetComponent<PickupSync>().additionalAmmo = p.additionalAmmo;
            //collectable.GetComponent<PickupSync>().type = p.type;

            //collectables.Add(p);
        }
    }

    Vector3 StringToVector(string _pos)
    {
        _pos = _pos.TrimEnd(')').TrimStart('(');
        Debug.Log("_pos: " + _pos);
        string[] pos = _pos.Split(',');
        Vector3 location = new Vector3(float.Parse(pos[0]), float.Parse(pos[1]), float.Parse(pos[2]));
        return location;
    }

    void OnAllPlayers(string recData)
    {
        string[] spData = recData.Split('|');
        for (int i = 1; i < spData.Length; i++)
        {
            string[] spD = spData[i].Split('%');

            Player p = new Player();

            p.connectionId = int.Parse(spD[0]);
            p.playerName = spD[1];
            p.prefabId = int.Parse(spD[2]);
            p.pos = new Vector3(float.Parse(spD[3]), 1.0f, float.Parse(spD[4]));
            p.rot = Quaternion.Euler(new Vector3(0.0f, float.Parse(spD[5]), 0.0f));
            p.weaponId = int.Parse(spD[6]);
            p.health = int.Parse(spD[7]);

            GameControllerScript.playerList.Add(p.connectionId, p);
        }
    }

    void OnNewPlayer(string recData)
    {
        string[] spData = recData.Split('|');

        Player p = new Player();

        p.connectionId = int.Parse(spData[1]);
        p.playerName = spData[2];
        p.prefabId = int.Parse(spData[3]);
        p.spawnPointId = int.Parse(spData[4]);
        p.weaponId = int.Parse(spData[5]);
        p.health = int.Parse(spData[6]);

        p.loses = 0;
        p.points = 0;

        GameControllerScript.playerList.Add(p.connectionId, p);
    }
}
