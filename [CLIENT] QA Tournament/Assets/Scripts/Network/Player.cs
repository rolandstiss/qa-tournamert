﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player
{ 
    public int connectionId;
    public int prefabId;
    public int weaponId;
    public int health;
    public int points;
    public int loses;
    public int spawnPointId;

    public string playerName;
    
    public GameObject avatar;

    public Vector3 pos;

    public Quaternion rot;
}
