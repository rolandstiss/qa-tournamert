﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup
{
    public int id;
    public int type;
    public int additionalAmmo;
    public bool isCollected;
    public Vector3 location;
}
