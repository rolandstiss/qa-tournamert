﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class HangarController : MonoBehaviour 
{
    AssetBundle myLoadedScenes;
    string[] scenePaths;

    NetworkManagerClient nm;
    TurntableScript tt;

    GameObject hangar;
    GameObject lobby;
    GameObject connecting;
    GameObject turntable;

    InputField pName;
    InputField serverIP;

    Text infoMsg;

    //Client Lobby text fields
    Text txtPID;
    Text txtPName;
    Text txtServerInfo;

    bool isConnecting = false;
    bool loadLevel = false;

    float connectionTimeOut = 10.0f;
    float timer;
    float connectionTimer;
    float interval = 5.0f;

    // Use this for initialization
    void Start () 
    {
        nm = GameObject.Find("NetworkManagerClient").GetComponent<NetworkManagerClient>();
        turntable = GameObject.Find("Turntable");
        tt = turntable.GetComponent<TurntableScript>();
        connecting = GameObject.Find("Connecting");

        //Client's Hangar
        hangar = GameObject.Find("Hangar");
        pName = hangar.gameObject.transform.Find("inPName").GetComponent<InputField>();
        serverIP = hangar.gameObject.transform.Find("inServerIP").GetComponent<InputField>();        

        //Client's Lobby
        lobby = GameObject.Find("Lobby");
        txtPID = lobby.transform.Find("Panel").transform.Find("txtPID").GetComponent<Text>();
        txtPName = lobby.transform.Find("Panel").transform.Find("txtPName").GetComponent<Text>();
        txtServerInfo = lobby.transform.Find("txtServerInfo").GetComponent<Text>();

        //info message
        infoMsg = GameObject.Find("txtInfoMsg").GetComponent<Text>();

        connecting.SetActive(false);
        lobby.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () 
    {
        //if the client is connected successfully - enter the lobby or join the match
        if (nm.isConnected)
        {
            connecting.SetActive(false);
            lobby.SetActive(true);

            txtPID.text = "pId\n";
            txtPName.text = "Player Name\n";

            foreach (KeyValuePair<int, Player> p in GameControllerScript.playerList)
            {
                txtPID.text += p.Value.connectionId + "\n";
                txtPName.text += p.Value.playerName + "\n";
            }
        }

        //return to hangar if there is a player with exact name on the server
        if (nm.nameExists && !nm.isConnected)
        {
            connecting.SetActive(false);
            hangar.SetActive(true);
            turntable.SetActive(true);
            connecting.SetActive(false);
            lobby.SetActive(false);

            infoMsg.color = Color.red;
            infoMsg.text = "Player with a name '" + pName.text + "' already exists on the server!";
        }
    }

    public void OnConnectToServer()
    {
        if (pName.text != "")
        {
            hangar.SetActive(false);
            turntable.SetActive(false);
            isConnecting = true;
            timer = Time.time;
            infoMsg.text = "";
            connecting.SetActive(true);
            nm.ConnectToServer(pName.text, tt.prefabId);
        }
        else
        {
            infoMsg.color = Color.red;
            infoMsg.text = "Please enter a Player Name";
        }
    }
}
