﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponInitialization : MonoBehaviour
{
    public string weaponName;
    public int type;
    public int ammo;

    public int damage;
    public int round;
    public int reserve;
    public int maxCapacity;

    public float reloadtime;
    public float fireRate;

    public bool isReloaded;
}
