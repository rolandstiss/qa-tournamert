﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float speed = 10.0f;

    Rigidbody playerRigidbody;
    int floorMask;
    Vector3 movement;


    private void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        playerRigidbody = GetComponent<Rigidbody>();
    }

    public void Turning()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit, 100f, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position; //Create Vector between player and mouse

            playerToMouse.y = 0f;

            Quaternion newRot = Quaternion.LookRotation(playerToMouse);

            playerRigidbody.MoveRotation(newRot);
        }
    }

    public void Move(float h, float v)
    {
        movement.Set(h, 0f, v);

        movement = movement.normalized * speed * Time.deltaTime;

        playerRigidbody.MovePosition(transform.position + movement);
    }
}
