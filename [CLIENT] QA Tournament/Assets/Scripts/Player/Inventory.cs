﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

    public List<WeaponClass> inventory;

	// Use this for initialization
	void Start () {
        inventory = new List<WeaponClass>();
        foreach (Transform t in transform.Find("WeaponHolder"))
        {
            WeaponInitialization wc = t.GetComponent<WeaponInitialization>();            
            inventory.Add(new WeaponClass(wc.name, wc.type, wc.ammo, wc.damage, wc.round, wc.reserve, wc.maxCapacity, wc.reloadtime, wc.fireRate, wc.isReloaded));
        }

        GameObject.Find("TextAmmo").GetComponent<WeaponAmmoText>().enabled = true;
	}
}
