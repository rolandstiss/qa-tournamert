﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    PlayerMovement plMovement;
    Shooting shoot;
    WeaponReload wr;
    Inventory inv;
    WeaponSwitching ws;
    NetworkManagerClient nm;

    Renderer rend;
    Collider coll;

    List<Transform> sp = new List<Transform>(); //Spawn points from GameControllerScript

    public int playerId;

    public float deathTime;

    public bool dead;

    float shootTimer;
    float deathTimer;

	// Use this for initialization
	void Start () {
        plMovement = GetComponent<PlayerMovement>();
        shoot = GetComponent<Shooting>();
        wr = GetComponent<WeaponReload>();
        inv = GetComponent<Inventory>();
        ws = GetComponent<WeaponSwitching>();
        nm = GameObject.Find("NetworkManagerClient").GetComponent<NetworkManagerClient>();

        rend = GetComponent<Renderer>();
        coll = GetComponent<Collider>();

        shootTimer = 0.0f;
        deathTime = 5.0f;
        dead = false;

        foreach (Transform s in GameObject.Find("GameController").GetComponent<LevelControllerScript>().spawnPoints)
        {
            sp.Add(s);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (GameControllerScript.playerList[playerId].health <= 0 && dead == false)
        {
            dead = true;
            deathTimer = Time.time;
            GameControllerScript.playerList[playerId].loses++;
            rend.enabled = false;
            coll.enabled = false;
            nm.SendToServer("PLAYERDOWN|" + playerId.ToString() + '|' + GameControllerScript.playerList[playerId].loses.ToString(), nm.myReliableChannel);
            //When player is down some animation or other notification for others should be sent from here!
        }

        if (deathTime + deathTimer < Time.time && GameControllerScript.playerList[playerId].health <= 0)
        {
            GameControllerScript.playerList[playerId].health = 100;
            GameControllerScript.playerList[playerId].avatar.transform.position = sp[Random.Range(0, sp.Count - 1)].position;
            GameControllerScript.playerList[playerId].avatar.transform.rotation = sp[Random.Range(0, sp.Count - 1)].rotation;
            rend.enabled = true;
            coll.enabled = true;
            nm.SendToServer("RESPAWN|" + playerId.ToString() + '|' + GameControllerScript.playerList[playerId].avatar.transform.position.x.ToString() + '|'
                                                                   + GameControllerScript.playerList[playerId].avatar.transform.position.z.ToString() + '|'
                                                                   + GameControllerScript.playerList[playerId].avatar.transform.rotation.eulerAngles.y.ToString(), nm.myReliableChannel);
            dead = false;
            //reset player health to server
        }

        if (GameControllerScript.playerList[playerId].health > 0)
        {
            if (Input.GetMouseButtonDown(0)
                && inv.inventory.ElementAt(ws.weaponId).ammo > 0
                && inv.inventory.ElementAt(ws.weaponId).fireRate + shootTimer <= Time.time
                && inv.inventory.ElementAt(ws.weaponId).isReloaded == true) // && something with reload time here
            {
                shootTimer = Time.time;
                shoot.OnLeftMouseClick(ws);
            }

            if (Input.GetKeyDown(KeyCode.R) &&
                                            (inv.inventory.ElementAt(ws.weaponId).isReloaded /*|| inv.inventory.ElementAt(ws.weaponId).reloadInterrupted*/) &&
                                            inv.inventory.ElementAt(ws.weaponId).ammo != inv.inventory.ElementAt(ws.weaponId).round &&
                                            inv.inventory.ElementAt(ws.weaponId).reserve > 0 ||
                                            (inv.inventory.ElementAt(ws.weaponId).ammo == 0 && inv.inventory.ElementAt(ws.weaponId).isReloaded))
            {
                inv.inventory.ElementAt(ws.weaponId).isReloaded = false;
                inv.inventory.ElementAt(ws.weaponId).reloadTimer = Time.time;
            }

            if (!inv.inventory.ElementAt(ws.weaponId).isReloaded)
                wr.Reloading(inv.inventory.ElementAt(ws.weaponId).reloadTimer, inv.inventory.ElementAt(ws.weaponId).reloadtime, ws.weaponId);

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                if (!inv.inventory.ElementAt(ws.weaponId).isReloaded)
                    inv.inventory.ElementAt(ws.weaponId).isReloaded = true;
                ws.EnableWeapon(0);
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                if (!inv.inventory.ElementAt(ws.weaponId).isReloaded)
                    inv.inventory.ElementAt(ws.weaponId).isReloaded = true;
                ws.EnableWeapon(1);
            }

            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                if (!inv.inventory.ElementAt(ws.weaponId).isReloaded)
                    inv.inventory.ElementAt(ws.weaponId).isReloaded = true;
                ws.EnableWeapon(2);
            }

            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                if (!inv.inventory.ElementAt(ws.weaponId).isReloaded)
                    inv.inventory.ElementAt(ws.weaponId).isReloaded = true;
                ws.EnableWeapon(3);
            }

            if (Input.mouseScrollDelta == new Vector2(0f, 1.0f))
            {
                if (!inv.inventory.ElementAt(ws.weaponId).isReloaded)
                    inv.inventory.ElementAt(ws.weaponId).isReloaded = true;
                ws.WeaponSwitch(++ws.weaponId);
            }

            if (Input.mouseScrollDelta == new Vector2(0f, -1.0f))
            {
                if (!inv.inventory.ElementAt(ws.weaponId).isReloaded)
                    inv.inventory.ElementAt(ws.weaponId).isReloaded = true;
                ws.WeaponSwitch(--ws.weaponId);
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GameControllerScript.playerList[playerId].health > 0)
        {
            float v = Input.GetAxisRaw("Vertical");
            float h = Input.GetAxisRaw("Horizontal");

            plMovement.Move(h, v);
            plMovement.Turning();
        }
    }
}
