﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WeaponSwitching : MonoBehaviour
{
    public int weaponId = 0;

    NetworkManagerClient nw;
    Inventory inv;

    public List<Transform> weapons;
    public List<Transform> barrelEnds;

    void Start ()
    {
        weapons = new List<Transform>();
        barrelEnds = new List<Transform>();
        nw = GameObject.Find("NetworkManagerClient").GetComponent<NetworkManagerClient>();
        inv = GetComponent<Inventory>();

        foreach (Transform w in transform.Find("WeaponHolder").transform)
        {
            weapons.Add(w);
            barrelEnds.Add(w.Find("BarrelEnd").transform);
        }

        EnableWeapon(weaponId);
    }

    public void WeaponSwitch(int wId)
    {
        if (wId > weapons.Count - 1)
            weaponId = 0;
        else if (wId < 0)
            weaponId = weapons.Count - 1;
        else
            weaponId = wId;
     
        EnableWeapon(weaponId);
    }

    public void EnableWeapon(int wId)
    {
        weaponId = wId;

        for (int i = 0; i < weapons.Count; i++)
        {
            if (i != weaponId)
                weapons.ElementAt(i).gameObject.SetActive(false);
            else
                weapons.ElementAt(i).gameObject.SetActive(true);
        }

        nw.SendToServer("SWWPN|" + weaponId.ToString(), nw.myUnreliableChannel);
    }
}
