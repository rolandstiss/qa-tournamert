﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Shooting : MonoBehaviour {

    float maxRayDistance = 100.0f;
    int layerMask;

    public int playerId;

    LevelControllerScript pr;
    WeaponSwitching ws;
    Inventory inv;
    WeaponReload wr;
    NetworkManagerClient nm;

	// Use this for initialization
	void Start () {
        ws = gameObject.GetComponentInChildren<WeaponSwitching>();
        inv = gameObject.GetComponentInChildren<Inventory>();
        wr = GetComponent<WeaponReload>();
        pr = GameObject.Find("LevelController").GetComponent<LevelControllerScript>();
        nm = GameObject.Find("NetworkManagerClient").GetComponent<NetworkManagerClient>();

        layerMask = LayerMask.NameToLayer("Enemy");
	}

    public void OnLeftMouseClick(WeaponSwitching sw)
    {
        Transform barrelEnd = sw.barrelEnds[ws.weaponId].transform;
        inv.inventory.ElementAt(ws.weaponId).ammo--;

        RaycastHit hit;

        Debug.Log(barrelEnd.position);

        if (Physics.Raycast(barrelEnd.position, barrelEnd.TransformDirection(Vector3.forward), out hit, maxRayDistance))
        {
            if (hit.collider.tag == "Wall")
            {
                Debug.Log("Wall is hit!!");
                GameObject p = Instantiate(pr.particles.ElementAt(0), hit.point, Quaternion.LookRotation(hit.normal)) as GameObject;
                //send to server needed particles
                SendParticleInfoToServer(0, hit);
            }
            else if (hit.collider.tag == "Enemy")
            {
                //Debug.Log("Enemy is hit!!");
                SendEnemyDamageToServer(hit.collider.GetComponent<EnemyController>().enemyId, inv.inventory.ElementAt(ws.weaponId).damage, playerId);
                GameObject p = Instantiate(pr.particles.ElementAt(1), hit.point, Quaternion.LookRotation(hit.normal)) as GameObject;
                //send needed particles to server
                SendParticleInfoToServer(1, hit);
            }
        }

        if (inv.inventory.ElementAt(ws.weaponId).ammo < 1)
        {
            inv.inventory.ElementAt(ws.weaponId).reloadTimer = Time.time;
            inv.inventory.ElementAt(ws.weaponId).isReloaded = false;
        }
    }

    void SendParticleInfoToServer(int partId, RaycastHit hitInfo)
    {
        string msg = "SPWNPART|" + nm.ourPlayerId + "|";
        msg += partId.ToString() + "|"; //mozh sho war smukaak
        msg += hitInfo.point.x.ToString() + "|";
        msg += hitInfo.point.y.ToString() + "|";
        msg += hitInfo.point.z.ToString() + "|";
        msg += Quaternion.LookRotation(hitInfo.normal).w.ToString() + "|";
        msg += Quaternion.LookRotation(hitInfo.normal).x.ToString() + "|";
        msg += Quaternion.LookRotation(hitInfo.normal).y.ToString() + "|";
        msg += Quaternion.LookRotation(hitInfo.normal).z.ToString();
        nm.SendToServer(msg, nm.myUnreliableChannel);
    }

    void SendEnemyDamageToServer(int enemyId, int damage, int pId)
    {
        string msg = "ENEMYHIT|" + enemyId.ToString() + '|' + damage.ToString() + '|' + pId.ToString();
        nm.SendToServer(msg, nm.myReliableChannel);
    }
}
