﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WeaponReload : MonoBehaviour {

    Inventory inv;
    //float timer;
    public bool isReloaded;

    // Use this for initialization
    void Start () {
        inv = GetComponent<Inventory>();
	}

    public void Reloading(float timer, float rlTime, int wpId)
    {
        if (timer + rlTime <= Time.time)
            ReloadWeapon(wpId);
    }

    public void ReloadWeapon(int wpId) // padomāt darbā!
    {
        if (inv.inventory.ElementAt(wpId).reserve - inv.inventory.ElementAt(wpId).round + inv.inventory.ElementAt(wpId).ammo > 0)
        {
            inv.inventory.ElementAt(wpId).reserve = inv.inventory.ElementAt(wpId).reserve - inv.inventory.ElementAt(wpId).round + inv.inventory.ElementAt(wpId).ammo;
            inv.inventory.ElementAt(wpId).ammo = inv.inventory.ElementAt(wpId).round;
        }

        else if (inv.inventory.ElementAt(wpId).reserve - inv.inventory.ElementAt(wpId).round + inv.inventory.ElementAt(wpId).ammo <= 0)
        {
            inv.inventory.ElementAt(wpId).ammo += inv.inventory.ElementAt(wpId).reserve;
            inv.inventory.ElementAt(wpId).reserve = 0;
        }

        inv.inventory.ElementAt(wpId).isReloaded = true;
        inv.inventory.ElementAt(wpId).reloadInterrupted = false;
    }
}
