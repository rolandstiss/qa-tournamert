﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponClass {

    public string name;
    public int type;
    public int ammo;
    public int damage;
    public int round;
    public int reserve;
    public int maxCapacity;

    public float reloadtime;
    public float fireRate;
    public float reloadTimer;

    public bool isReloaded;
    public bool reloadInterrupted;

    public WeaponClass(string _name, int _type, int _ammo, int _damage, int _round, int _reserve, int _maxCapacity, float _reloadTime, float _firerate, bool _isReloaded)
    {
        name = _name;
        type = _type;
        ammo = _ammo;
        damage = _damage;
        round = _round;
        reserve = _reserve;
        maxCapacity = _maxCapacity;
        reloadtime = _reloadTime;
        fireRate = _firerate;
        isReloaded = _isReloaded;
        reloadTimer = 0.0f;
        reloadInterrupted = false;
    }
}
