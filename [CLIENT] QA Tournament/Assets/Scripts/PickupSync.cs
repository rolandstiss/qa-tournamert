﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PickupSync : MonoBehaviour {

    public int id;
    public float speed;
    public int additionalAmmo;
    public int type;

    bool collected;

    Collider coll;
    Renderer rend;
    NetworkManagerClient nm;
    Inventory inv;

	// Use this for initialization
	void Start () {
        speed = 50.0f;
        coll = GetComponent<Collider>();
        rend = GetComponent<Renderer>();
        nm = GameObject.Find("NetworkManagerClient").GetComponent<NetworkManagerClient>();
        inv = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        collected = nm.collectables.ElementAt(id-1).isCollected;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (nm.collectables.ElementAt(id-1).isCollected == true)
        {
            coll.enabled = false;
            rend.enabled = false;
        }

        else if (!nm.collectables.ElementAt(id-1).isCollected && !coll.enabled)
        {
            coll.enabled = true;
            rend.enabled = true;
        }

        transform.Rotate(Vector3.up * speed * Time.deltaTime);
	}

    private void OnTriggerEnter(Collider other)
    {        
        if (other.tag == "Player")
        {
            inv.inventory.Find(x => x.type == type).reserve += additionalAmmo;
            string msg = "ISCOLLECTED|" + id.ToString();
            nm.SendToServer(msg, nm.myReliableChannel);
        }
    }
}
