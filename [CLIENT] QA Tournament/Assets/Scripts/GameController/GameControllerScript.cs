﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControllerScript : MonoBehaviour {

    public Text scoreText;
    public Text timeText;

    public static Dictionary<int, Player> playerList;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        playerList = new Dictionary<int, Player>();
    }
}
