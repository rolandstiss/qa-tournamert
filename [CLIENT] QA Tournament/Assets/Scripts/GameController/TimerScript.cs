﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour {

    int mm = 9;
    int ss = 59;

    int minutes;
    int seconds;

    bool timeIsSynced = false;
    int offsetTime;

    Text timerText;

    // Use this for initialization
    void Start () {
        timerText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!timeIsSynced)
            return;

        timerText.text = timerUpdate((int)Time.time + offsetTime);
	}

    public void SetTime(float t)
    {
        offsetTime = (int)(t - Time.time);
        //Debug.Log("offsettime: " + offsetTime);
        timeIsSynced = true;
    }

    string timerUpdate(int t)
    {
        minutes = mm - t / 60;
        seconds = ss - t % 60;    

        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
