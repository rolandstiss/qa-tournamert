﻿using System.Collections;
using System.Collections.Generic;
using System.Linq; 
using UnityEngine;

public class WeaponSync : MonoBehaviour {

    public int weaponId = 0;
    public List<Transform> weapons;

    public int cnnId;
    NetworkManagerClient wp;

    // Use this for initialization
    public void Start()
    {
        weapons = new List<Transform>();
        cnnId = gameObject.GetComponentInParent<EnemyController>().enemyId;
        //Debug.Log("enemy cnnId: " + NetworkManagerClient.playerList[cnnId].weaponId);

        Debug.Log("weaponId: " + weaponId);
        Debug.Log("dict weaponId: " + GameControllerScript.playerList[cnnId].weaponId);

        foreach (Transform w in transform)
        {
            weapons.Add(w);
        }

        EnableWeapon(0);
    }

    private void Update()
    {
        if (weaponId != GameControllerScript.playerList[cnnId].weaponId)
        {
            Debug.Log("should switch weapon if the weaponId != 0 ");
            EnableWeapon(GameControllerScript.playerList[cnnId].weaponId);
        }
    }

    void EnableWeapon(int wId)
    {
        weaponId = wId;

        for (int i = 0; i < weapons.Count; i++)
        {
            if (i != weaponId)
                weapons.ElementAt(i).gameObject.SetActive(false);
            else
                weapons.ElementAt(i).gameObject.SetActive(true);
        }
    }
}
