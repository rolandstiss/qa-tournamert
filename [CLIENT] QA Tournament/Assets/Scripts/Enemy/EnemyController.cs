﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public int enemyId; //this one will be the main id, must be moved from Interpolation!!!
    public int health;

    public bool dead;

    Renderer rend;
    Collider coll;

	// Use this for initialization
	void Start () {
        dead = false;
        rend = GetComponent<Renderer>();
        coll = GetComponent<Collider>();
	}
	
	// Update is called once per frame
	void Update () {
        health = GameControllerScript.playerList[enemyId].health;

        if (GameControllerScript.playerList[enemyId].health <= 0)
        {
            dead = true;
            rend.enabled = false;
            coll.enabled = false;
        }

        else if (GameControllerScript.playerList[enemyId].health > 0 && dead == true)
        {
            dead = false;
            rend.enabled = true;
            coll.enabled = true;
        }            
	}
}
