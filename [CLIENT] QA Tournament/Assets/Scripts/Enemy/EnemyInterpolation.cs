﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInterpolation : MonoBehaviour {

    EnemyController ec;
    public float lerpTime = 10;

    private void Start()
    {
        ec = GetComponent<EnemyController>();
    }

    // Update is called once per frame
    void Update () {
        //Debug.Log("Interpolation cnnId: " + cnnId);
        transform.position = Vector3.Lerp(transform.position, GameControllerScript.playerList[ec.enemyId].pos, lerpTime * Time.fixedDeltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, GameControllerScript.playerList[ec.enemyId].rot, lerpTime * Time.fixedDeltaTime);
    }
}
