﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestroyer : MonoBehaviour {

    public float lifetime;

	// Use this for initialization
	void Start ()
    {
        gameObject.GetComponent<ParticleSystem>().Play();
        Destroy(gameObject, lifetime);
	}
}
