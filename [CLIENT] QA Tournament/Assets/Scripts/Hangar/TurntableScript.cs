﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurntableScript : MonoBehaviour {

    public GameObject[] playerPrefabs;

    public int prefabId = 0;
    float dist = 5.0f;
    float smooth = 15.0f;

    float rotOffset;
    float newRot;

	// Use this for initialization
	void Start ()
    {
        rotOffset = 360 / playerPrefabs.Length;

        foreach (GameObject p in playerPrefabs)
        {
            GameObject pl = Instantiate(p, transform.position, Quaternion.identity) as GameObject;
            pl.transform.Rotate(-Vector3.up * rotOffset * prefabId);
            pl.transform.Translate(Vector3.forward * dist);
            pl.transform.parent = gameObject.transform;
            //pl.transform.Find("WeaponHolder").GetComponent<WeaponInitialization>().gameObject.SetActive(false);
            prefabId++;
        }

        newRot = 0.0f;
        prefabId = 0;
	}
	
	// Update is called once per frame
	void Update () {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, newRot, 0), smooth * Time.deltaTime);
	}

    public void TurnRight()
    {
        prefabId--;
        if (prefabId < 0)
            prefabId = playerPrefabs.Length - 1;

        newRot = rotOffset * prefabId;
        //Debug.Log(prefabId);
    }

    public void TurnLeft()
    {
        prefabId++;
        if (prefabId > playerPrefabs.Length - 1)
            prefabId = 0;

        newRot = rotOffset * prefabId;
        //Debug.Log(prefabId); ;
    }
}
