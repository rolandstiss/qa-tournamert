﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeScript : MonoBehaviour {

    public Text timeText;

    float timer;
    float offsetTime;

    int mm;
    int ss;

    public int minutes;
    public int seconds;

	// Use this for initialization
	void Start () {
        timeText = GetComponent<Text>();
        timer = Time.timeSinceLevelLoad;

        mm = 9;
        ss = 59;
	}
	
	// Update is called once per frame
	void Update () {
        timeText.text = GameTimeUpdate(Time.timeSinceLevelLoad);
	}

    public string GameTimeUpdate(float timer)
    {
        minutes = mm - (int)timer / 60;
        seconds = ss - (int)timer % 60;

        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
