﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerClient
{
    public int connectionId;
    public int prefabId;
    public int weaponId;
    public int health;
    public int points;
    public int loses;
    public int spawnpointId;

    public string playerName;

    public bool levelLoaded;
    
    public Vector3 pos;

    public Quaternion rot;
}
