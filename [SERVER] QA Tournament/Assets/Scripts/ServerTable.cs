﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ServerTable : MonoBehaviour {

    string pId = "pId";
    string pName = "Player";
    string loses = "Loses";
    string points = "Points";
    string space = "     ";

    Text tableText;

	// Use this for initialization
	void Start () {
        tableText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        string dataOutput = pId.PadRight(3) + space + pName.PadRight(15) + space + loses.PadRight(3) + space + points.PadRight(3) + "\n\n";
        foreach (KeyValuePair<int, ServerClient> sc in NetworkManagerServer.serverClients)
        {
            dataOutput += sc.Value.connectionId.ToString().PadRight(3) + space + sc.Value.playerName.PadRight(15) + space + sc.Value.loses.ToString().PadRight(5) + space + sc.Value.points.ToString().PadRight(3) + "\n";
        }

        dataOutput.TrimEnd();

        tableText.text = dataOutput;
	}
}
