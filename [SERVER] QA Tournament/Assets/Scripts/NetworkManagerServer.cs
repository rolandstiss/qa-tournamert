﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class NetworkManagerServer : MonoBehaviour {

    //network related variables
    const int MAX_CONNECTIONS = 20;
    int port = 5071;
    string connectionIP = "127.0.0.1";

    int hostId;
    public int myReliableChannel;
    public int myUnreliableChannel;

    byte error;

    //update rates
    float updateTimer = 0.1f; //position
    float notImportantUpdates = 1.0f; //update rate for lobby statuses and other not that neccessary stuff
    float timer;

    //server variables
    int spawnPoint = 0;
    public bool serverIsStarted = false;

    public bool itsLobby = false;
    public bool itsMatch = false;

    TimeScript gameTime;

    int waitingTime = 3;

    public static Dictionary<int, ServerClient> serverClients = new Dictionary<int, ServerClient>();

    //pickup locations are defined in Pickups.xml file
    public const string pickupXML = @"D:\Personal Projects\[GAME] QA Tournament\[SERVER] QA Tournament\Assets\Resources\Pickups.xml";// should try that super path
    public PickupContainer pc = new PickupContainer();

    //spawnpoint locations are defined in Spawnpoints.xml file
    public const string spawnpointXML = @"D:\Personal Projects\[GAME] QA Tournament\[SERVER] QA Tournament\Assets\Resources\Spawnpoints.xml";// should try that super path
    public SpawnpointContainer sp = new SpawnpointContainer();

    // Update is called once per frame
    void Update ()
    {
        if (serverIsStarted)
        {
            DataReceive();

            if (notImportantUpdates + timer < Time.time)
            {
                string message = "PLAYERSTATS|";
                foreach (KeyValuePair<int, ServerClient> sc in serverClients)
                {
                    message += sc.Key + '%' + sc.Value.playerName + '|';
                }

                message.Trim('|');

                SendToAllClients(message, myUnreliableChannel, serverClients);
            }

            //if (updateTimer + timer < Time.time)
            //{
            //    string msg = "ASKPOSITION|";
            //    foreach (KeyValuePair<int, ServerClient> sc in serverClients)
            //    {
            //        msg += sc.Value.connectionId.ToString() + '%' + sc.Value.pos.x + '%' + sc.Value.pos.z + '%' + sc.Value.rot.eulerAngles.y + '|';
            //    }

            //    msg = msg.Trim('|');
            //    SendToAllClients(msg, myUnreliableChannel, serverClients);

            //    timer = Time.time;
            //}

            //foreach (Pickup p in pc.pickups)
            //{
            //    if (p.isCollected == true && p.timer + p.cooldownTime < Time.time)
            //    {
            //        p.isCollected = false;
            //        ReenablePickup(p.id);
            //    }
            //}

            //if (updateTimer + timer < Time.time)
            //{
            //    SendToAllClients("JUSTMSG|", myReliableChannel, serverClients);
            //    timer = Time.time;
            //}
        }
        
    }

    public void DataReceive()
    {
        int recHostId;
        int connectionId;
        int channelId;
        byte[] recBuffer = new byte[1024];
        int bufferSize = 1024;
        int dataSize;
        byte error;
        NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
        switch (recData)
        {
            case NetworkEventType.Nothing: break;

            case NetworkEventType.ConnectEvent:
                OnConnection(connectionId);
                break;

            case NetworkEventType.DataEvent:
                string data = Encoding.Unicode.GetString(recBuffer);
                string[] splitData = data.Split('|');

                switch (splitData[0])
                {
                    case "NAMEIS":
                        //check is the name unique
                        // 1-cnnId, 2-pName, 3-prefId
                        if (!CheckPlayerName(int.Parse(splitData[1]), splitData[2], serverClients))
                        {
                            //if unique - add player to the player list
                            OnNameIs(int.Parse(splitData[1]), splitData[2], int.Parse(splitData[3]));
                            //SendPickupInitialization(connectionId);
                            //tell everyone new player's pos, rot, name, prefabId
                        }

                        else
                            SendToClient("NAMEEXISTS|", int.Parse(splitData[1]), myReliableChannel);
                        break;

                    case "LEVELLOADED":
                        Debug.Log("LEVELLOADED");
                        serverClients[connectionId].levelLoaded = true;
                        if (ClientLevelLoaded())
                            Debug.Log("EveryoneIsReady");
                            //SendToAllClients("SPWNPLAYERS|" + )
                        //check all players and set player level status in class
                        break;

                        //case "MYPOSITION":
                        //    OnMyPosition(splitData);
                        //    break;

                        //case "SWWPN":
                        //    //Debug.Log("SWWPN: " + int.Parse(splitData[1]));
                        //    serverClients[connectionId].weaponId = int.Parse(splitData[1]);
                        //    SendToAllClients("WPNSW|" + connectionId + '|' + int.Parse(splitData[1]), myUnreliableChannel, serverClients);
                        //    break;

                        //case "SPWNPART":
                        //    //Debug.Log("Some particles arrived: " + splitData[1] + "// vector: " + splitData[2] + "// quaternion: " + splitData[3]);
                        //    SendToAllClients("SPWNPART|" + splitData[1] + "|" + splitData[2] + "|" + splitData[3] + "|" + splitData[4] + "|" + splitData[5] + "|" + splitData[6] + "|" + splitData[7] + "|" + splitData[8] + "|" + splitData[9], myUnreliableChannel, serverClients);
                        //    break;

                        //case "ENEMYHIT":
                        //    serverClients[int.Parse(splitData[1])].health -= int.Parse(splitData[2]);

                        //    if (serverClients[int.Parse(splitData[1])].health <= 0)
                        //        serverClients[int.Parse(splitData[3])].points++;

                        //    SendToAllClients("DAMAGE|" + int.Parse(splitData[1]) + '|' + serverClients[int.Parse(splitData[1])].health, myReliableChannel, serverClients);
                        //    break;

                        //case "RESPAWN":
                        //    serverClients[int.Parse(splitData[1])].pos = new Vector3(float.Parse(splitData[2]), 0.0f, float.Parse(splitData[3]));
                        //    serverClients[int.Parse(splitData[1])].rot = Quaternion.Euler(0.0f, float.Parse(splitData[4]), 0.0f);
                        //    serverClients[int.Parse(splitData[1])].health = 100;
                        //    SendToAllClients("RESPAWN|" + splitData[1] + '|' + serverClients[int.Parse(splitData[1])].pos.x.ToString() +
                        //                                                 '|' + serverClients[int.Parse(splitData[1])].pos.z.ToString() +
                        //                                                 '|' + serverClients[int.Parse(splitData[1])].rot.eulerAngles.y.ToString() +
                        //                                                 '|' + serverClients[int.Parse(splitData[1])].health, myUnreliableChannel, serverClients);
                        //    break;

                        //case "PLAYERDOWN":
                        //    serverClients[int.Parse(splitData[1])].loses = int.Parse(splitData[2]);
                        //    SendToAllClients("PLAYERDOWN|" + splitData[1], myReliableChannel, serverClients);
                        //    break;

                        //case "ISCOLLECTED":
                        //    Debug.Log("ISCOLLECTED");
                        //    OnIsCollected(int.Parse(splitData[1]));
                        //    break;
                }

                break;

            case NetworkEventType.DisconnectEvent:
                RemoveFromList(connectionId);
                break;

            case NetworkEventType.BroadcastEvent: break;
        }
    }

    public void StartServer()
    {
        NetworkTransport.Init();

        ConnectionConfig config = new ConnectionConfig();
        myReliableChannel = config.AddChannel(QosType.Reliable);
        myUnreliableChannel = config.AddChannel(QosType.Unreliable);

        HostTopology topology = new HostTopology(config, MAX_CONNECTIONS);

        hostId = NetworkTransport.AddHost(topology, port, null);

        //gameTime = GameObject.Find("timeText").GetComponent<TimeScript>();
        serverClients.Clear(); // reset client list
        timer = Time.time;

        pc = PickupContainer.Load(pickupXML);
        sp = SpawnpointContainer.Load(spawnpointXML);
        serverIsStarted = true;
    }

    public void RestartMatch()
    { }

    //disconnect all players, clear the dictionary, change server state and remove the host, return to server setup
    public void ShutDownServer()
    {
        if(serverClients.Count > 0)
            SendToAllClients("PDC|", myReliableChannel, serverClients);
        Debug.Log("PDC");
        StartCoroutine(ShutDownServ());
        serverIsStarted = false;
        NetworkTransport.RemoveHost(hostId);
    }

    void OnConnection(int cnnId)
    {
        string msg = "ASKNAME|" + cnnId.ToString() + '|';

        msg = msg.Trim('|');
        SendToClient(msg, cnnId, myReliableChannel);
    }

    public void SendToClient(string message, int cnnId, int channelId)
    {
        byte[] msg = Encoding.Unicode.GetBytes(message);
        NetworkTransport.Send(hostId, cnnId, channelId, msg, message.Length * sizeof(char), out error);
    }

    public void SendToAllClients(string message, int channelId, Dictionary<int, ServerClient> c)
    {
        if (c.Count > 0)
        {
            byte[] msg = Encoding.Unicode.GetBytes(message);
            foreach (KeyValuePair<int, ServerClient> sc in c)
            {
                NetworkTransport.Send(hostId, sc.Value.connectionId, channelId, msg, message.Length * sizeof(char), out error);
            }
        }        
    }

    void OnNameIs(int cnnId, string pName, int prefId) //must add Vector3 and Quaternion for pos and rot after the spawn point is assigned
    {
        if (++spawnPoint > 3)
            spawnPoint = 0;

        string msg;

        //Send old players a message about new player
        msg = "NEWPLAYER|" + cnnId + '|' + pName + '|' + prefId + '|' + spawnPoint + '|' + 0 + '|' + 100;
        SendToAllClients(msg, myReliableChannel, serverClients);

        ServerClient p = new ServerClient();
        p.connectionId = cnnId;
        p.playerName = pName;
        p.prefabId = prefId;
        p.pos = new Vector3(0.0f, 1.0f, 0.0f); //msut be synced with spawn point
        p.rot = Quaternion.identity; //must be synced with spawn point
        p.weaponId = 0;
        p.loses = 0;
        p.points = 0;
        p.health = 100;
        serverClients.Add(cnnId, p);

        msg = "ALLPLAYERS|";

        //Send freshly connected player full player list
        foreach (KeyValuePair<int, ServerClient> sc in serverClients)
        {
            msg += sc.Value.connectionId.ToString() + '%' + sc.Value.playerName.ToString() + '%' + sc.Value.prefabId.ToString() + '%' + sc.Value.pos.x.ToString() + '%' + sc.Value.pos.z.ToString() + '%' + sc.Value.rot.y.ToString() + '%' + sc.Value.weaponId + '%' + sc.Value.health + '|';
        }
        msg = msg.Trim('|');
        SendToClient(msg, cnnId, myReliableChannel);

        if (itsMatch)
        {
            SendToClient("MATCHSTART|" + 3, cnnId, myReliableChannel);
        }
    }

    void RemoveFromList(int cnnId)
    {        
        string msg = "DC|" + cnnId;
        SendToAllClients(msg, myReliableChannel, serverClients);
        serverClients.Remove(cnnId);
        //Must tell to others about someone leaving!!!
        //Debug.Log("player " + cnnId + "has disconnected");
    }

    void OnMyPosition(string[] data)
    {
        serverClients[int.Parse(data[1])].pos = new Vector3(float.Parse(data[2]), 1.0f, float.Parse(data[3]));
        serverClients[int.Parse(data[1])].rot = Quaternion.Euler(0.0f, float.Parse(data[4]), 0.0f);
    }

    void SendPickupInitialization(int cnnId)
    {
        string msg = "SPAWNPICKUPS|";
        foreach (Pickup p in pc.pickups)
        {
            msg += p.id.ToString() + '%' + p.type.ToString() + '%' + p.isCollected.ToString() + '%' + p.loc + '%' + p.additionalAmmo.ToString() + '|';
        }

        msg = msg.TrimEnd('|');

        SendToClient(msg, cnnId, myReliableChannel);
    }

    void OnIsCollected(int id)
    {
        //store pickup change in the dictionary!!!
        pc.pickups.Find(x => x.id == id).isCollected = true;
        pc.pickups.Find(x => x.id == id).timer = Time.time;
        Debug.Log("DISABLEPICKUP");
        SendToAllClients("DISABLEPICKUP|" + id, myReliableChannel, serverClients);
    }

    void ReenablePickup(int id)
    {
        SendToAllClients("REENABLEPICKUP|" + id, myReliableChannel, serverClients);
    }

    bool CheckPlayerName(int cnnId, string newPName, Dictionary<int, ServerClient> sc)
    {
        bool nameExists = false;
        foreach (KeyValuePair<int, ServerClient> s in sc)
        {
            if (s.Value.playerName == newPName)
            {
                nameExists = true;
                Debug.Log("nameexists: " + nameExists);
                break;
            }
        }

        return nameExists;
    }

    IEnumerator ShutDownServ()
    {
        yield return new WaitForSecondsRealtime(waitingTime);

        serverIsStarted = false;
        NetworkTransport.RemoveHost(hostId);
    }

    bool ClientLevelLoaded()
    {
        bool ready = true;

        foreach (KeyValuePair<int, ServerClient> sc in serverClients)
        {
            if (sc.Value.levelLoaded != true)
            {
                ready = false;
                break;
            }
        }

        return ready;
    }
}
