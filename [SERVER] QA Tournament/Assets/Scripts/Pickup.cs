﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

[XmlRoot("PickupCollection")]
public class Pickup
{
    [XmlAttribute("id")]
    public int id;

    [XmlElement("type")]
    public int type;

    [XmlElement("cooldowntime")]
    public float cooldownTime;

    [XmlElement("additionalammo")]
    public int additionalAmmo;

    [XmlElement("collected")]
    public bool isCollected;

    [XmlElement("loc")]
    public string loc;

    //public Vector3 location;

    public float timer = 0.0f;

    //public Vector3 StringToVector(string _pos)
    //{
    //    string[] pos = _pos.Split(',');
    //    Vector3 location = new Vector3(float.Parse(pos[0]), float.Parse(pos[1]), float.Parse(pos[2]));
    //    return location;
    //}
}