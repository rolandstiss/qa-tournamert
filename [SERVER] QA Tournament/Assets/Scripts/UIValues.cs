﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIValues : MonoBehaviour 
{
    GameObject setup;
    GameObject lobby;

    Dropdown mapId;
    Text mapIdText;

    Text tVal;
    Slider tSlider;

    Text minPVal;
    Slider minPSlider;

    Text maxPVal;
    Slider maxPSlider;

    Text infoMsg;

    NetworkManagerServer nm;

    Text connectedPlayers;
    Text waitingInfo;

    Text cnnId;
    Text pName;
    Text points;
    Text loses;
    Text IP;
    Text latency;
    Text prefab;

    int t;
    int minP;
    int maxP;

    bool matchStarted = false;
    float waitingTime = 5.0f;

    private void Start()
    { 
        setup = GameObject.Find("Setup");
        lobby = GameObject.Find("Lobby");
        mapId = GameObject.Find("ddMap").GetComponent<Dropdown>();
        mapIdText = GameObject.Find("txtMapId").GetComponent<Text>();
        tVal = GameObject.Find("txtTValue").GetComponent<Text>();
        tSlider = GameObject.Find("sliderT").GetComponent<Slider>();
        minPVal = GameObject.Find("txtMinPlayerValue").GetComponent<Text>();
        minPSlider = GameObject.Find("sliderMinPlayer").GetComponent<Slider>();
        maxPVal = GameObject.Find("txtMaxPlayerValue").GetComponent<Text>();
        maxPSlider = GameObject.Find("sliderMaxPlayer").GetComponent<Slider>();

        nm = GameObject.Find("NetworkManagerServer").GetComponent<NetworkManagerServer>();

        connectedPlayers = GameObject.Find("txtConnectedPlayers").GetComponent<Text>();
        cnnId = GameObject.Find("txtCnnId").GetComponent<Text>();
        pName = GameObject.Find("txtName").GetComponent<Text>();
        points = GameObject.Find("txtPoints").GetComponent<Text>();
        loses = GameObject.Find("txtLoses").GetComponent<Text>();
        IP = GameObject.Find("txtIP").GetComponent<Text>();
        latency = GameObject.Find("txtLatency").GetComponent<Text>();
        prefab = GameObject.Find("txtPrefabId").GetComponent<Text>();
        waitingInfo = GameObject.Find("txtWaitingInfo").GetComponent<Text>();

        infoMsg = GameObject.Find("infoMsg").GetComponent<Text>();

        lobby.SetActive(false);
        infoMsg.enabled = true;
    }

    private void Update()
    {
        if (!nm.serverIsStarted)
        {
            lobby.SetActive(false);
            setup.SetActive(true);            

            //update match time txt
            if (t != (int)tSlider.value)
            {
                t = (int)tSlider.value;
                tVal.text = t + " min";
            }

            //update min player amount txt
            if (minP != (int)minPSlider.value)
            {
                minPVal.text = minPSlider.value.ToString();
            }

            //update max player amount txt
            if (maxP != (int)maxPSlider.value)
            {
                maxPVal.text = maxPSlider.value.ToString();
            }
        }

        else if (nm.serverIsStarted)
        {
            UpdateTable(); //this could be updated once per second or something like that!!!

            if (NetworkManagerServer.serverClients.Count < (int)minPSlider.value)
            {
                waitingInfo.text = "Waiting for min player amount";
            }

            else if (NetworkManagerServer.serverClients.Count < (int)minPSlider.value && waitingTime <= 0)
            {
                waitingInfo.color = Color.red;
                waitingInfo.text = "Waiting for min player amount";
            }

            else if (NetworkManagerServer.serverClients.Count >= (int)minPSlider.value && waitingTime <= 0 && !nm.itsMatch)
            {
                nm.itsMatch = true;
                nm.SendToAllClients("MATCHSTART|" + 3, nm.myReliableChannel, NetworkManagerServer.serverClients); //3 should be replaced with "mapId.value"

                waitingInfo.color = Color.green;
                waitingInfo.text = "The match has started!";
            }

            //wait for all players to load the level or 10s
            else if (nm.itsMatch && IsEveryoneReady(NetworkManagerServer.serverClients))
            {
                nm.SendToAllClients("STARTBATTLE|", nm.myReliableChannel, NetworkManagerServer.serverClients);
            }

            else
            {
                waitingTime -= Time.deltaTime;
                waitingInfo.text = "Match will start in " + (int)waitingTime + " s";
            }



            //display text

        }
    }

    void UpdateTable()
    {
        cnnId.text = "cnnId\n";
        pName.text = "Name\n";
        points.text = "Points\n";
        loses.text = "Loses\n";
        IP.text = "IP\n";
        latency.text = "Latency\n";
        prefab.text = "Prefab\n";

        connectedPlayers.text = "Connected players: " + NetworkManagerServer.serverClients.Count.ToString() + "/" + maxP.ToString() + " (min " + minP.ToString() + ")";

        foreach (KeyValuePair<int, ServerClient> sc in NetworkManagerServer.serverClients)
        {
            cnnId.text += sc.Value.connectionId + "\n";
            pName.text += sc.Value.playerName + "\n";
            points.text += sc.Value.points + "\n";
            loses.text += sc.Value.loses + "\n";
            prefab.text += sc.Value.prefabId + "\n";
        }
    }

    public void OnStartClick()
    {
        StartServer(mapId.value, (int)tSlider.value, (int)minPSlider.value, (int)maxPSlider.value);
    }

    public void OnRestartClick()
    {
        foreach (KeyValuePair<int, ServerClient> sc in NetworkManagerServer.serverClients)
        {
            sc.Value.health = 100;
            sc.Value.points = 0;
            sc.Value.loses = 0;
            //resend spawn coordinates to each clients
            //restart countdown
            //restart timer
        }
    }

    public void StartServer(int mId, int t, int _minP, int _maxP)
    {
        if (_maxP >= _minP)
        {
            //server starts here with the parameters
            minP = _minP;
            maxP = _maxP;
            nm.StartServer();
            mapIdText.text = "Map ID: " + mId.ToString();
            connectedPlayers.text = "Connected players: " + NetworkManagerServer.serverClients.Count.ToString() + "/" + maxP.ToString() + " (min " + minP.ToString() + ")";
            setup.SetActive(false);
            lobby.SetActive(true);

            infoMsg.color = Color.green;
            infoMsg.text = "The server has been started!";
        }
        else
        {
            infoMsg.text = "min Player Amount cannot be bigger than max Player Amount!";
        }
    }

    bool IsEveryoneReady(Dictionary<int, ServerClient> sc)
    {
        bool everyOneIsReady = true;

        foreach (KeyValuePair<int, ServerClient> s in sc)
        {
            if (s.Value.levelLoaded != true)
            {
                everyOneIsReady = false;
                break;
            }
        }

        return everyOneIsReady;
    }
}
