﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("PickupCollection")]
public class PickupContainer{

    [XmlArray("Pickups")]
    [XmlArrayItem("Pickup")]
    public List<Pickup> pickups = new List<Pickup>();

    public static PickupContainer Load(string path)
    {
        TextAsset _xml = Resources.Load<TextAsset>(path);

        XmlSerializer s = new XmlSerializer(typeof(PickupContainer));

        var reader = new FileStream(path, FileMode.Open);

        PickupContainer pickups = s.Deserialize(reader) as PickupContainer;

        reader.Close();

        return pickups;
    }
}
