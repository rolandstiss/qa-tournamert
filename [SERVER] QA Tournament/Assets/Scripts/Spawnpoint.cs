﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

[XmlRoot("SpawnpointCollection")]
public class Spawnpoint
{
    [XmlAttribute("id")]
    public int id;

    [XmlElement("taken")]
    public bool taken;

    [XmlElement("positionX")]
    public float posX;

    [XmlElement("positionY")]
    public float posY;

    [XmlElement("positionZ")]
    public float posZ;

    //public Quaternion rot;    
}
