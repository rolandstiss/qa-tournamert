﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("SpawnpointCollection")]
public class SpawnpointContainer
{

    [XmlArray("Spawnpoints")]
    [XmlArrayItem("Spawnpoint")]
    public List<Pickup> spawnpoints = new List<Pickup>();

    public static SpawnpointContainer Load(string path)
    {
        TextAsset _xml = Resources.Load<TextAsset>(path);

        XmlSerializer s = new XmlSerializer(typeof(SpawnpointContainer));

        var reader = new FileStream(path, FileMode.Open);

        SpawnpointContainer spawnpoints = s.Deserialize(reader) as SpawnpointContainer;

        reader.Close();

        return spawnpoints;
    }
}