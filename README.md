##About this project

A server-client study with Unity which was not finished since TransportLayer was depricated.
The project consist of a placeholder lavel (client) and a lobby (server).

##How to launch this

Download project from git, open two Unity instances - one with Server and other with Client project
First launch Server from editor or standalone build
Second launch different clients - it is not possible to have the same names

##Outcome

Theoretically working server-client prototype, sufficien for turn based games.
Lacking movement prediction, therefore fps are not going to work with this.
